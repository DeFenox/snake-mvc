package com.defenox.snake.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by tokarev on 27.06.2017.
 */
public class StartStopListener implements ActionListener {
    private Controller controller;

    /**
     *
     * @param controller type GameController
     */
    public StartStopListener(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("Start")){
            controller.pressStart();
        }else if(e.getActionCommand().equals("Stop")){
            controller.pressStop();
        }
    }
}
