package com.defenox.snake.controller;

import com.defenox.snake.model.GameField;
import com.defenox.snake.view.GameView;

import java.awt.event.MouseEvent;

/**
 * Created by DeFenox on 04.06.2017.
 */
public class Controller {
    private GameField model;
    private GameView view;

    public Controller(GameField model, GameView view) {
        this.model = model;
        this.view = view;
    }

    public void pressStart() {
        view.pressStart();
        model.pressStart();
    }

    public void pressStop() {
        view.pressStop();
        model.pressStop();

    }

    public void pressPause() {
        view.pressPause();
        model.pressPause();

    }

    public void pressResume() {
        view.pressResume();
        model.pressResume();
    }

    public void snakeTurn(MouseEvent e) {
        model.snakeTurn(e);
    }
}