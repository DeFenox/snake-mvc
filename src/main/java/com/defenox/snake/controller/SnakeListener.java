package com.defenox.snake.controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by tokarev on 27.06.2017.
 */
public class SnakeListener implements MouseListener {
    private Controller controller;

    /**
     *
     * @param controller type GameController
     */
    public SnakeListener(Controller controller) {
        this.controller = controller;
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(MouseEvent e) {
        this.controller.snakeTurn(e);
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(MouseEvent e) {

    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public void mouseEntered(MouseEvent e) {

    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public void mouseExited(MouseEvent e) {

    }
}
