package com.defenox.snake;

import com.defenox.snake.controller.Controller;
import com.defenox.snake.controller.PauseResumeListener;
import com.defenox.snake.controller.StartStopListener;
import com.defenox.snake.model.GameField;
import com.defenox.snake.view.GameView;

import java.util.*;

/**
 * Created by DeFenox on 04.06.2017.
 */
public class App {
    private static final int AMOUNT_INPUT_PARAMS = 7;
    private Controller controller;
    private GameField gameField;
    private GameView gameView;
    private Map<String, Integer> params;

    public App(){
        gameField = new GameField();
        gameView = new GameView(gameField);
        controller = new Controller(gameField, gameView);

        gameView.addStartStopButtonListener(new StartStopListener(controller));
        gameView.addPauseResumeButtonListener(new PauseResumeListener(controller));
        //gameView.addStopButtonListener(new StopListener(controller));

    }


    public void setArgs(String[] args) {

        params = getDefaultParamsMap(args);

        initWidth(params.get("width"));
        initHeight(params.get("height"));
        initSnakeLength(params.get("lengthOfSnake"));
        initTimeSnakeSleep(params.get("sleepOfSnake"));
        initAmountFrogRed(params.get("amountRedFrogs"));
        initAmountFrogGreen(params.get("amountGreenFrogs"));
        initAmountFrogBlue(params.get("amountBlueFrogs"));

        gameField.setParamsOfGame(params);

    }

    private void initAmountFrogGreen(Integer amountGreenFrogs) {
        try {
            if (amountGreenFrogs < 1) {
                throw new IllegalArgumentException();
            } else {
                params.put("amountGreenFrogs", amountGreenFrogs);
            }
        } catch (NumberFormatException ex) {
            initAmountFrogGreen(Integer.valueOf(gameView.displayErrorDialog("You need to enter integers", "1")));
        } catch (IllegalArgumentException ex) {
            initAmountFrogGreen(Integer.valueOf(gameView.displayErrorDialog("Amount of blue frogs can't be less 1", "1")));
        }
    }

    private void initAmountFrogBlue(Integer amountBlueFrogs) {
        try {
            if (amountBlueFrogs < 1) {
                throw new IllegalArgumentException();
            } else {
                params.put("amountBlueFrogs", amountBlueFrogs);
            }
        } catch (NumberFormatException ex) {
            initAmountFrogBlue(Integer.valueOf(gameView.displayErrorDialog("You need to enter integers", "1")));
        } catch (IllegalArgumentException ex) {
            initAmountFrogBlue(Integer.valueOf(gameView.displayErrorDialog("Amount of green frogs can't be less 1", "1")));
        }
    }

    private void initAmountFrogRed(Integer amountRedFrogs) {
        try {
            if (amountRedFrogs < 1) {
                throw new IllegalArgumentException();
            } else {
                params.put("amountRedFrogs", amountRedFrogs);
            }
        } catch (NumberFormatException ex) {
            initAmountFrogRed(Integer.valueOf(gameView.displayErrorDialog("You need to enter integers", "1")));
        } catch (IllegalArgumentException ex) {
            initAmountFrogRed(Integer.valueOf(gameView.displayErrorDialog("Amount of red frogs can't be less 1", "1")));
        }
    }

    private void initTimeSnakeSleep(Integer sleepOfSnake) {
        try {
            if (sleepOfSnake < 500) {
                throw new IllegalArgumentException();
            } else {
                params.put("sleepOfSnake", sleepOfSnake);
            }
        } catch (NumberFormatException ex) {
            initTimeSnakeSleep(Integer.valueOf(gameView.displayErrorDialog("You need to enter integers", "500")));
        } catch (IllegalArgumentException ex) {
            initTimeSnakeSleep(Integer.valueOf(gameView.displayErrorDialog("Time of snake sleep can't be less 500", "500")));
        }
    }

    private void initSnakeLength(Integer lengthOfSnake) {
        try {
            if (lengthOfSnake < 2) {
                throw new IllegalArgumentException();
            } else {
                params.put("lengthOfSnake", lengthOfSnake);
            }
        } catch (NumberFormatException ex) {
            initSnakeLength(Integer.valueOf(gameView.displayErrorDialog("You need to enter integers", "2")));
        } catch (IllegalArgumentException ex) {
            initSnakeLength(Integer.valueOf(gameView.displayErrorDialog("Length of snake can't be less 2", "2")));
        }
    }

    private void initHeight(Integer height) {
        try {
            if (height < 10) {
                throw new IllegalArgumentException();
            } else {
                params.put("height", height);
            }
        } catch (NumberFormatException ex) {
            initHeight(Integer.valueOf(gameView.displayErrorDialog("You need to enter integers", "10")));
        } catch (IllegalArgumentException ex) {
            initHeight(Integer.valueOf(gameView.displayErrorDialog("Height of board can't be less 10", "10")));
        }
    }

    private void initWidth(Integer width) {
        try {
            if (width < 10) {
                throw new IllegalArgumentException();
            } else {
                params.put("width", width);
            }
        } catch (NumberFormatException ex) {
            initWidth(Integer.valueOf(gameView.displayErrorDialog("You need to enter integers", "10")));
        } catch (IllegalArgumentException ex) {
            initWidth(Integer.valueOf(gameView.displayErrorDialog("Width of board can't be less 10", "10")));
        }
    }


    /**
              *
              * @param args array params. Order of priority <br/> <br/> <br/>
            *             <b>width</b> - of game field per block, not be less 10 <br/>
            *             <b>height</b> - of game field per block, not be less 10 <br/>
            *             <b>snake's length</b>, not be less 2<br/>
            *             <b>snake's time to sleep</b>, not be less 100<br/>
            *             <b>amount of red frog</b>, not be less 1<br/>
            *             <b>amount of green frog</b>, not be less 1<br/>
            *             <b>amount of blue frog</b>, not be less 1<br/>
            */

    private Map<String, Integer> getDefaultParamsMap(String[] args) {

        List<String> argsArray = new ArrayList<String>(Arrays.asList(args));

        Map<String,Integer> params = new HashMap<String, Integer>();
        putParameterIntoMap("width", 0, args, params);
        putParameterIntoMap("height", 1, args, params);
        putParameterIntoMap("lengthOfSnake", 2, args, params);
        putParameterIntoMap("sleepOfSnake", 3, args, params);
        putParameterIntoMap("amountRedFrogs", 4, args, params);
        putParameterIntoMap("amountGreenFrogs", 5, args, params);
        putParameterIntoMap("amountBlueFrogs", 6, args, params);

        return params;
    }

    private void putParameterIntoMap(String str, int integer, String[] args, Map<String, Integer> params) {
        try {
            params.put(str, Integer.valueOf(args[integer]));
        }catch (IndexOutOfBoundsException e){
            params.put(str, 0);
        }
    }

    //TODO
    public void start() {
        gameView.init(controller);
        //controller.pressStart();
    }
}
