package com.defenox.snake.model.enumeration;

import com.defenox.snake.model.GameField;
import com.defenox.snake.model.Part;

/**
 * Created by DeFenox on 04.06.2017.
 */
public enum Direction {
    UP, RIGHT, DOWN, LEFT;

    public static Direction turnLeft(Direction direction){
        switch (direction){
            case UP:
                direction = LEFT;
                break;
            case RIGHT:
                direction = UP;
                break;
            case DOWN:
                direction = RIGHT;
                break;
            case LEFT:
                direction = DOWN;
                break;
            default:
                direction = RIGHT;
                break;
        }
        return direction;
    }

    public static Direction turnRight(Direction direction){
        switch (direction){
            case UP:
                direction = RIGHT;
                break;
            case RIGHT:
                direction = DOWN;
                break;
            case DOWN:
                direction = LEFT;
                break;
            case LEFT:
                direction = UP;
                break;
            default:
                direction = RIGHT;
                break;
        }
        return direction;
    }


    public static Part getNextCoordinate(Direction direction, Part partIn, GameField gameField) {
        Part part = new Part(partIn);
        switch (direction) {
            case UP:
                part = moveUp(partIn, gameField);
                break;
            case RIGHT:
                part = moveRight(partIn, gameField);
                break;
            case DOWN:
                part = moveDown(partIn, gameField);
                break;
            case LEFT:
                part = moveLeft(partIn, gameField);
                break;
        }
        return part;
    }

    public static Part moveUp(Part partIn, GameField gameField) {
        Part part = new Part(partIn);
        if (part.getY() == 0) {
            part.setY(gameField.getHeight() - 1);
        } else {
            part.decY();
        }
        return part;
    }

    public static Part moveRight(Part partIn, GameField gameField) {
        Part part = new Part(partIn);
        if (part.getX() == gameField.getWidth() - 1) {
            part.setX(0);
        } else {
            part.incX();
        }
        return part;
    }

    public static Part moveDown(Part partIn, GameField gameField) {
        Part part = new Part(partIn);
        if (part.getY() == gameField.getHeight() - 1) {
            part.setY(0);
        } else {
            part.incY();
        }
        return part;
    }

    public static Part moveLeft(Part partIn, GameField gameField) {
        Part part = new Part(partIn);
        if (part.getX() == 0) {
            part.setX(gameField.getWidth() - 1);
        } else {
            part.decX();
        }
        return part;
    }
}
