package com.defenox.snake.model.enumeration;

/**
 * Created by DeFenox on 25.06.2017.
 */
public enum UpdateType {
    UPDATE_SCORE, UPDATE_BUTTON_AFTER_SNAKE_DIE, UPDATE_FIELD, UPDATE_ALL
}
