package com.defenox.snake.model;

/**
 * Created by DeFenox on 04.06.2017.
 */
public interface Pause {
    void resume();

    void suspend();
}
