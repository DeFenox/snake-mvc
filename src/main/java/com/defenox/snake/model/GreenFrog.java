package com.defenox.snake.model;

/**
 * Created by DeFenox on 11.06.2017.
 */
public class GreenFrog extends Frog {

    private static final int SCORE = 1;

    public GreenFrog(GameField gameField) {
        super(gameField);
    }

    public void die() {
        super.getGameField().frogDie(this);
        super.getGameField().addScore(SCORE);
        super.getGameField().getSnake().expand();
    }
}
