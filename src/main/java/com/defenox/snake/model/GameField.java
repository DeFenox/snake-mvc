package com.defenox.snake.model;

import com.defenox.snake.model.enumeration.Direction;
import com.defenox.snake.model.enumeration.UpdateType;

import java.awt.event.MouseEvent;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by DeFenox on 04.06.2017.
 */
public class GameField extends Observable {

    private int width;
    private int height;

    private int score;

    private int lengthOfSnake;
    private int sleepOfSnake;

    private int redFrog;
    private int greenFrog;
    private int blueFrog;

    private volatile Snake snake;
    private volatile List<Frog> frogs = new CopyOnWriteArrayList<>();
    private ExecutorService service;

    public List<Frog> getFrogs() {
        return frogs;
    }

    public void setChange() {
        this.setChanged();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Snake getSnake() {
        return snake;
    }

    public Part getHeadSnake() {
        return snake.getHead();
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
        this.setChanged();
        this.notifyObservers(UpdateType.UPDATE_SCORE);
    }

    public void addScore(int score) {
        this.setScore(this.score += score);
    }

    public void setParamsOfGame(Map<String, Integer> params) {
        width = params.get("width");
        height = params.get("height");
        lengthOfSnake = params.get("lengthOfSnake");
        sleepOfSnake = params.get("sleepOfSnake");
        redFrog = params.get("amountRedFrogs");
        greenFrog = params.get("amountGreenFrogs");
        blueFrog = params.get("amountBlueFrogs");

        service = Executors.newFixedThreadPool(redFrog + greenFrog + blueFrog);
    }

    public void pressStart() {
        score = 0;
        createSnake();
        createFrogs();
    }

    public void pressStop() {
        dieSnake();
        dieFrogs();
    }

    public void pressPause() {
        suspendSnake();
        suspendFrogs();
    }

    public void pressResume() {
        resumeSnake();
        resumeFrogs();
    }

    private void createSnake() {
        this.snake = new Snake(this, lengthOfSnake);
        snake.setMovementInterval(sleepOfSnake);
        new Thread(snake).start();
    }

    private void createFrogs() {
        frogs.clear();

        for (int i = 0; i < redFrog; i++) {
            createFrog(new RedFrog(this));
        }
        for (int i = 0; i < greenFrog; i++) {
            createFrog(new GreenFrog(this));
        }
        for (int i = 0; i < blueFrog; i++) {
            createFrog(new BlueFrog(this));
        }
    }

    private void createFrog(Frog frog) {
        frog.setMovementInterval(snake.getMovementInterval() * 2);
        frog.setBodyFromPart(getGeneratedEmptyCoordinate());
        frogs.add(frog);
        service.execute(frog);
        this.setChanged();
        this.notifyObservers(UpdateType.UPDATE_FIELD);
    }

    private Part getGeneratedEmptyCoordinate() {
        Random dice = new Random();

        Part part;

        do {
            int x = dice.nextInt(width);
            int y = dice.nextInt(height);

            part = new Part(x, y);
        } while (hasCollisionWithFrogs(part) || hasCollisionWithWholeSnake(part));

        return part;
    }

    private boolean hasCollisionWithFrogs(Part part) {

        boolean hasCollision = false;
        if(frogs != null) {
            for (Frog frog : frogs) {
                if (frog.getBody().get(0).equals(part)) {
                    hasCollision = true;
                }
            }
        }
        return hasCollision;
    }

    private boolean hasCollisionWithWholeSnake(Part part) {
        boolean hasCollision = false;
        List<Part> body = snake.getBody();
        for (Part partOfBody : body) {
            if (partOfBody.equals(part)) {
                hasCollision = true;
            }
        }
        return hasCollision;
    }

    private void dieSnake() {
        snake.setAlive(false);
        snake.resume();
    }

    public void dieFrogs() {
        for (Frog frog : frogs) {
            frog.setAlive(false);
            frog.resume();
        }
    }

    public void frogDie(Frog frogDie) {
        frogDie.setBodyFromPart(getGeneratedEmptyCoordinate());
        this.setChanged();
        this.notifyObservers(UpdateType.UPDATE_ALL);
    }

    private void suspendSnake() {
        snake.suspend();
    }

    private void suspendFrogs() {
        for (Frog frog : frogs) {
            frog.suspend();
        }
    }

    private void resumeSnake() {
        snake.resume();
    }

    private void resumeFrogs() {
        for (Frog frog : frogs) {
            frog.resume();
        }
    }

    public List<Part> getListAvailableCoordinatesForFrog(Part part) {
        List<Part> enableCells = getListNextCells(part);
        enableCells = removeNotAvailableCells(enableCells);
        return enableCells;
    }

    private List<Part> getListNextCells(Part part) {
        List<Part> enableCells = new ArrayList<>();
        enableCells.add(Direction.moveUp(part, this));
        enableCells.add(Direction.moveRight(part, this));
        enableCells.add(Direction.moveDown(part, this));
        enableCells.add(Direction.moveLeft(part, this));
        return enableCells;
    }

    private List<Part> removeNotAvailableCells(List<Part> enableCells) {
        List<Part> cells = new ArrayList<>();
        List<Part> found = new ArrayList<>();
        cells.addAll(enableCells);
        for(Part cell : cells){
            if (hasCollisionWithFrogs(cell) || hasCollisionWithSnakeHead(cell)) {
                found.add(cell);
            }
        }
        cells.removeAll(found);
        return cells;
    }

    private boolean hasCollisionWithSnakeHead(Part part) {

        return part.equals(getHeadSnake());
    }

    public void eatFrogBySnake(){
        Part snakeHead = snake.getHead();
        if(frogs != null) {
            for (Frog frog : frogs) {
                if (frog.getBody().get(0).equals(snakeHead)) {
                    frog.die();
                }
            }
        }
    }

    public void snakeTurn(MouseEvent e) {
        if(!snake.isCanTurn())
            return;

        snake.setCanTurn(false);
        if (e.getButton() == MouseEvent.BUTTON1) {
            snake.turnLeft();
        } else if (e.getButton() == MouseEvent.BUTTON3) {
            snake.turnRight();
        }
    }


    /*public Part getNextCoordinate(Direction direction, Part partIn) {
        Part part = new Part(partIn);
        switch (direction) {
            case UP:
            part = moveUp(part);
            break;
            case RIGHT:
            part = moveRight(part);
            break;
            case DOWN:
            part = moveDown(part);
            break;
            case LEFT:
            part = moveLeft(part);
        break;
        }
        return part;
    }*/

    /*
    private Part moveUp(Part part) {
        if (part.getY() == 0) {
            part.setY(height - 1);
        } else {
            part.decY();
        }
        return part;
}

    private Part moveRight(Part part) {
        if (part.getX() == width - 1) {
            part.setX(0);
        } else {
            part.incX();
        }
        return part;
    }

    private Part moveDown(Part part) {
        if (part.getY() == height - 1) {
            part.setY(0);
        } else {
            part.incY();
        }
        return part;
    }

    private Part moveLeft(Part part) {
        if (part.getX() == 0) {
            part.setX(width - 1);
        } else {
            part.decX();
        }
        return part;
    }*/
}
