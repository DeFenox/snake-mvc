package com.defenox.snake.model;

/**
 * Created by DeFenox on 11.06.2017.
 */
public class BlueFrog extends Frog {

    public BlueFrog(GameField gameField) {
        super(gameField);
    }

    public void die() {
        super.getGameField().frogDie(this);
        super.getGameField().getSnake().die();
    }
}
