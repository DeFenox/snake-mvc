package com.defenox.snake.model;

import com.defenox.snake.model.enumeration.UpdateType;

import java.util.List;
import java.util.Random;

/**
 * Created by DeFenox on 04.06.2017.
 */
public abstract class Frog extends Person{

    private int chanceAwayFromSnakeHead = 80; // per cent

    public Frog(GameField gameField) {
        super(gameField);
    }

    public abstract void die();

    public void move() {
        List<Part> body = super.getBody();
        Part tmpPart = getNextStep(body.get(0));
        if(tmpPart != null) {
            body.get(0).setX(tmpPart.getX());
            body.get(0).setY(tmpPart.getY());
        }
        getGameField().setChange();
        getGameField().notifyObservers(UpdateType.UPDATE_FIELD);
    }

    private Part getNextStep(Part part) {
        List<Part> availableCoordinate = getGameField().getListAvailableCoordinatesForFrog(part);
        if(availableCoordinate.isEmpty()){
            return null;
        }else  if(availableCoordinate.size() == 1){
            return availableCoordinate.get(0);
        }else {
            return getCalculatedNextStepFarFromSnake(availableCoordinate);
        }
    }

    private Part getCalculatedNextStepFarFromSnake(List<Part> availableCoordinateList) {
        Part snakeHead = getGameField().getHeadSnake();
        int index = getIndexCloseForSnake(availableCoordinateList, snakeHead);
        Random dice = new Random();
        int chance = dice.nextInt(100) + 1;
        if(chance <= chanceAwayFromSnakeHead){
            availableCoordinateList.remove(index);
            return availableCoordinateList.get(dice.nextInt(availableCoordinateList.size()));
        }else {
            return availableCoordinateList.get(index);
        }
    }

    private int getIndexCloseForSnake(List<Part> availableCoordinate, Part snakeHead) {
        int index = -1;
        float minDistance = Float.MAX_VALUE;
        for (int i = 0; i < availableCoordinate.size(); i++){
            float tmpDistance = lengthBetweenPoints(availableCoordinate.get(i), snakeHead);
            if(minDistance > tmpDistance){
                minDistance = tmpDistance;
                index = i;
            }
        }
        return index;
    }

    public void setBodyFromPart(Part part){
        List<Part> body = super.getBody();
        body.clear();
        body.add(part);
    }

    private float lengthBetweenPoints(Part partFist, Part partSecond){
        return (float) Math.sqrt(Math.abs(Math.pow(Math.abs(partSecond.getX() - partFist.getX()), 2) - Math.pow(Math.abs(partSecond.getY() - partFist.getY()), 2)));
    }
}
