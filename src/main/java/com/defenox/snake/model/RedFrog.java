package com.defenox.snake.model;

/**
 * Created by DeFenox on 11.06.2017.
 */
public class RedFrog extends Frog {

    private static final int SCORE = 2;

    public RedFrog(GameField gameField) {
        super(gameField);
    }

    public void die() {
        super.getGameField().frogDie(this);
        super.getGameField().addScore(SCORE);
        super.getGameField().getSnake().reduction();
    }
}
