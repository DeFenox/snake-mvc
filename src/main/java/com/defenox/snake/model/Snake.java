package com.defenox.snake.model;

import com.defenox.snake.model.enumeration.Direction;
import com.defenox.snake.model.enumeration.UpdateType;

import java.util.List;

public class Snake extends Person {

    private int length = 2;
    private Part previousStepOfTail;
    private Direction direction;

    private boolean canTurn;

    public Snake(GameField gameField, int length) {
        super(gameField);
        setLength(length);
        setDefaultBodyPositions();
        direction = Direction.RIGHT;
    }

    public Part getHead(){
        return super.getBody().get(0);
    }

    public boolean isCanTurn() {
        return canTurn;
    }

    public void setCanTurn(boolean canTurn) {
        this.canTurn = canTurn;
    }

    public void setLength(int length) {
        if(length < 2) {
            throw new IllegalArgumentException("Length of snake can't less by 2");
        }
        this.length = length;
    }

    private void setDefaultBodyPositions() {
        List<Part> body = super.getBody();
        for (int i = length - 1; i >= 0; i--) {
            body.add(new Part(i, 0));
        }
    }

    public void die() {
        super.setAlive(false);
        getGameField().dieFrogs();
        getGameField().setChange();
        getGameField().notifyObservers(UpdateType.UPDATE_BUTTON_AFTER_SNAKE_DIE);
    }

    public void expand() {
        List<Part> body = super.getBody();
        body.add(previousStepOfTail);
        length++;
    }

    public void reduction() {
        if(length > 2) {
            List<Part> body = super.getBody();
            this.previousStepOfTail = body.get(body.size() - 1);
            body.remove(body.size() - 1);
            length--;
        }
    }

    public void move() {
        List<Part> body = super.getBody();
        previousStepOfTail = body.get(body.size() - 1);
        for (int i = body.size() - 1; i > 0; i-- ) {
            body.set(i,body.get(i - 1));
        }
        body.set(0, Direction.getNextCoordinate(direction, body.get(0), this.getGameField()));

        if(hasCollisionWithBody()){
            die();
        }

        attemptEatFrog();

        if(!canTurn){
            canTurn = true;
        }
        getGameField().setChange();
        getGameField().notifyObservers(UpdateType.UPDATE_FIELD);
    }

    private void attemptEatFrog() {
        getGameField().eatFrogBySnake();
    }

    private boolean hasCollisionWithBody() {
        List<Part> body = getBody();
        for (int i = 1; i < body.size(); i++) {
            if (body.get(i).equals(body.get(0))) {
                return true;
            }
        }
        return false;
    }

    public void turnLeft() {
        direction = Direction.turnLeft(direction);
    }

    public void turnRight() {
        direction = Direction.turnRight(direction);
    }
}
