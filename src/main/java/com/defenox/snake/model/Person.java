package com.defenox.snake.model;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by DeFenox on 04.06.2017.
 */
abstract class Person implements Runnable, Pause {

    private GameField gameField;
    private volatile List<Part> body = new CopyOnWriteArrayList<>();

    private volatile boolean alive;
    private volatile boolean pause;
    private int movementInterval;
    private final Object pauseLock = new Object();

    public Person(GameField gameField) {
        this.gameField = gameField;
        this.alive = true;
    }

    abstract void die();

    public void run() {
        while (isAlive()) {
            try {
                move();

                Thread.sleep(this.movementInterval);
                synchronized (pauseLock) {
                    while (isPause()) try {
                        pauseLock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } catch (InterruptedException e) {
            }
        }
    }

    @Override
    public void resume() {
        synchronized (pauseLock) {
            pause = false;
            pauseLock.notify();
        }
    }

    @Override
    public void suspend() {
        pause = true;
    }

    abstract public void move();

    public boolean isPause() {
        return pause;
    }

    public List<Part> getBody() {
        return body;
    }

    public void setBody(List<Part> body) {
        this.body = body;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public GameField getGameField() {
        return gameField;
    }

    public int getMovementInterval() {
        return movementInterval;
    }

    public void setMovementInterval(int movementInterval) {
        this.movementInterval = movementInterval;
    }
}
