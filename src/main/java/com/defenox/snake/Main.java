package com.defenox.snake;


/**
 * Created by DeFenox on 04.06.2017.
 */
public class Main {

    public static void main(String[] args) {

        App app = new App();

        app.setArgs(args);

        app.start();

    }
}
