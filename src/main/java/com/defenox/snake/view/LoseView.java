package com.defenox.snake.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by DeFenox on 29.07.2017.
 */
public class LoseView extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 320;
    private static final int FRAME_HEIGHT = 240;


    private JLabel loseLabel = new JLabel("You lose!!!");
    private JLabel scoreLabel = new JLabel("You score: ");
    private JPanel panel = new JPanel();

    private JButton okButton = new JButton("OK");

    public LoseView(String score){

        this.setLayout(new BorderLayout());

        this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setMinimumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        this.setMaximumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        this.getRootPane().setDoubleBuffered(true);
        //this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Score");

        //set position window on center of screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        this.setVisible(true);

        initLoseLabel();

        this.scoreLabel.setText( "You score: " + score);

        panel.setLayout(new GridLayout(3, 1, 0, 3));

        okButton.addActionListener(this);

        panel.add(loseLabel, BorderLayout.CENTER);
        panel.add(scoreLabel, BorderLayout.CENTER);
        panel.add(okButton, BorderLayout.CENTER);

        this.add(panel, BorderLayout.CENTER);
    }

    private void initLoseLabel() {
        loseLabel.setFont(new Font("Arial", Font.BOLD, 18));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.dispose();
    }
}
