package com.defenox.snake.view;

import com.defenox.snake.controller.Controller;
import com.defenox.snake.controller.SnakeListener;
import com.defenox.snake.model.GameField;
import com.defenox.snake.model.enumeration.UpdateType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by DeFenox on 04.06.2017.
 */
public class GameView extends JFrame implements Observer{

    private static final int FRAME_WIDTH = 640;
    private static final int FRAME_HEIGHT = 480;

    private GameField gameField;

    private JButton startStopButton = new JButton("Start");
    private JButton pauseResumeButton = new JButton("Pause");
    //private JButton stopButton = new JButton("Stop");

    private JLabel scoreLabel = new JLabel("Score: ");
    private JLabel pointLabel = new JLabel("000000");

    private JPanel framePanel = new JPanel();
    private JPanel gamePanel = new JPanel();
    private JPanel buttonPanel = new JPanel();
    private JPanel scorePanel = new JPanel();
    private GameFieldView gameFieldView;


    private JScrollPane scrollPane = new JScrollPane();
    private JViewport viewport = new JViewport();

    private SnakeListener snakeListener;

    public GameView(GameField gameField) {
        this.gameField = gameField;
        this.gameFieldView = new GameFieldView(gameField);
        gameField.addObserver(this);
    }


    public void init(Controller controller){
        this.setLayout(new BorderLayout());

        this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setMinimumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        this.setMaximumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        this.getRootPane().setDoubleBuffered(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Snake");

        //set position window on center of screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        this.setVisible(true);
        Container containPane = this.getContentPane();
        containPane.add(framePanel, BorderLayout.CENTER);
        framePanel.setLayout(new BorderLayout());

        gamePanel.setLayout(new GridLayout(2, 1, 0, 3));
        framePanel.add(gamePanel, BorderLayout.NORTH);

        buttonPanel.setLayout(new FlowLayout());

        scorePanel.setLayout(new FlowLayout());

        gamePanel.add(buttonPanel);
        gamePanel.add(scorePanel);

        buttonPanel.add(startStopButton);
        buttonPanel.add(pauseResumeButton);

        startStopButton.setActionCommand("Start");
        pauseResumeButton.setActionCommand("Pause");
        //buttonPanel.add(stopButton);

        pauseResumeButton.setEnabled(false);
        //stopButton.setEnabled(false);

        scorePanel.add(scoreLabel);
        scorePanel.add(pointLabel);

        //gameFieldView.addMouseListener(mouseListener);

        snakeListener = new SnakeListener(controller);

        viewport.add(gameFieldView);

        scrollPane.setViewport(viewport);


        framePanel.add(scrollPane, BorderLayout.CENTER);
    }

    /**
     *
     * @param errorMessage string error massage
     * @param defaultValue string default value
     * @return string of enter data
     */
    public String displayErrorDialog(String errorMessage, String defaultValue){
        return JOptionPane.showInputDialog(this, errorMessage, defaultValue);
    }

    /**
     *
     * @param formattedPoint string of point
     */
    public void setPointLabel(String formattedPoint){
        this.pointLabel.setText(formattedPoint);
        this.pointLabel.updateUI();
    }

    public void addStartStopButtonListener(ActionListener listenerForStartStopButton){
        startStopButton.addActionListener(listenerForStartStopButton);
    }

    public void addPauseResumeButtonListener(ActionListener listenerForPauseResumeButton){
        pauseResumeButton.addActionListener(listenerForPauseResumeButton);
    }

    public void pressStart(){
        startStopButton.setActionCommand("Stop");
        startStopButton.setText("Stop");
        pauseResumeButton.setEnabled(true);
        gameFieldView.addMouseListener(snakeListener);
        updateScore(0);
    }

    public void pressStop(){
        startStopButton.setActionCommand("Start");
        startStopButton.setText("Start");
        pauseResumeButton.setEnabled(false);
        gameFieldView.removeMouseListener(snakeListener);
    }

    public void pressPause(){
        pauseResumeButton.setActionCommand("Resume");
        pauseResumeButton.setText("Resume");
    }

    public void pressResume(){
        pauseResumeButton.setActionCommand("Pause");
        pauseResumeButton.setText("Pause");
    }

    public void update(Observable observable, Object arg) {
        if (arg.equals(UpdateType.UPDATE_FIELD)) {
            updateGameField();
        }else if (arg.equals(UpdateType.UPDATE_BUTTON_AFTER_SNAKE_DIE)) {
            pressStop();
            new LoseView(Integer.toString(gameField.getScore()));
        }else if (arg.equals(UpdateType.UPDATE_SCORE)) {
            updateScore(gameField.getScore());
        }else if (arg.equals(UpdateType.UPDATE_ALL)) {
            updateScore(gameField.getScore());
            updateGameField();
        }
    }

    private void updateScore(int score) {
        String formatted = String.format("%06d", score);
        setPointLabel(formatted);
    }

    private void updateGameField() {
        gameFieldView.draw();
    }
}
