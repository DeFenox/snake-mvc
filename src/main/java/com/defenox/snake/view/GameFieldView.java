package com.defenox.snake.view;

import com.defenox.snake.model.*;

import javax.imageio.ImageIO;


import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by tokarev on 27.06.2017.
 */
public class GameFieldView extends Canvas {

    public static final int WIDTH_IMAGE = 20;
    public static final int HEIGHT_IMAGE = 20;
    public static final int SPACE = 0;
    private GameField gameField;

    private BufferedImage blueFrog;
    private BufferedImage greenFrog;
    private BufferedImage redFrog;

    private BufferedImage snakeHead;
    private BufferedImage snakeBody;
    private BufferedImage snakeTail;

    private BufferedImage tile;

    private BufferStrategy bs;
    private Graphics g;

    public GameFieldView(GameField gameField) {
        this.gameField = gameField;

        initializeImages();
    }

    private void initializeImages() {
        //refactor this
        this.blueFrog = getImage("/../../../../image/frog_blue.gif");
        this.greenFrog = getImage("/../../../../image/frog_green.gif");
        this.redFrog = getImage("/../../../../image/frog_red.gif");

        this.snakeHead = getImage("/../../../../image/snake_head.gif");
        this.snakeBody = getImage("/../../../../image/snake_body.gif");
        this.snakeTail = getImage("/../../../../image/snake_tail.gif");

        this.tile = getImage("/../../../../image/tile.gif");
    }

    public void draw() {
        if (bs == null) {
            createBufferStrategy(2); // 2 - double buffer
        }

        bs = getBufferStrategy();
        g = bs.getDrawGraphics();

        drawField(g);
        drawFrogs(g);
        drawSnake(g);

        g.dispose();
        bs.show();

        try {
            Thread.sleep(3);
        } catch (InterruptedException e) {
            System.out.printf("Something was wrong!");
        }

    }

    private BufferedImage getImage(String path) {
        try {
            //System.out.println(this.getClass().getResource(getClass().getName()));
            InputStream in = getClass().getResourceAsStream(path);
            return ImageIO.read(in);
        } catch (IOException e) {
            System.out.printf("File not found");
        }
        return null;
    }

    private void drawField(Graphics g) {
        for (int y = 0; y < gameField.getWidth(); y++) {
            for (int x = 0; x < gameField.getHeight(); x++) {
                drawElement(tile, x, y, g);
            }
        }
    }

    private void drawElement(BufferedImage image, int x, int y, Graphics g) {
        g.drawImage(image, x * (WIDTH_IMAGE + SPACE), y * (HEIGHT_IMAGE + SPACE), WIDTH_IMAGE, HEIGHT_IMAGE, null);
    }

    private void drawFrogs(Graphics g) {
        for (Frog frog : gameField.getFrogs()) {
            if(frog instanceof BlueFrog){
                drawElement(blueFrog, frog.getBody().get(0).getX(), frog.getBody().get(0).getY(), g);
            }else if(frog instanceof RedFrog){
                drawElement(redFrog, frog.getBody().get(0).getX(), frog.getBody().get(0).getY(), g);
            }else if(frog instanceof GreenFrog){
                drawElement(greenFrog, frog.getBody().get(0).getX(), frog.getBody().get(0).getY(), g);
            }
        }
    }

    private void drawSnake(Graphics g) {
        java.util.List<Part> body = gameField.getSnake().getBody();
        if(body == null){
            return;
        }

        for (int i = 0; i < body.size(); i++) {
            if(i == 0){
                drawElement(snakeHead, body.get(i).getX(), body.get(i).getY(), g);
            }else if(i == body.size() - 1){
                drawElement(snakeTail, body.get(i).getX(), body.get(i).getY(), g);
            }else{
                drawElement(snakeBody, body.get(i).getX(), body.get(i).getY(), g);
            }
        }
    }
}
