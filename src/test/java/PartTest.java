import com.defenox.snake.model.Part;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by DeFenox on 10.06.2017.
 */
public class PartTest {

    Part part = new Part(0, 0);

    @Test
    public void equals() throws Exception {
        assertEquals("Parts are not equals", true, part.equals(new Part(0, 0)));
        assertEquals("Parts are equals", false, part.equals(new Part(1, 0)));
        assertEquals("Parts are equals", false, part.equals(new Part(0, 1)));
        assertEquals("Parts are equals", false, part.equals(new Part(1, 1)));
    }

}